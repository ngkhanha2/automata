package Automata.Output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TextOutput {
	public void outputToFile(String text, String fileName) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
		out.write(text);
		out.close();
	}
}
