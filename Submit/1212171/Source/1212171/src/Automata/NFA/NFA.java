package Automata.NFA;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.swing.text.StyledEditorKit.ForegroundAction;

import Automata.Automata;
import Automata.Simulator;

public class NFA extends Automata {

	private int exitState = -1;

	public NFA() {
		super(new HashMap<Integer, List<NFATransition>>());
	}

	public void createExitState() {
		int state = 0;
		for (int key : ((Map<Integer, List<NFATransition>>) this
				.getTransitions()).keySet()) {
			if (key > state) {
				state = key;
			}
		}
		createExitState(state + 1);
	}

	private List<NFATransition> getTransitionsOfState(int state) {
		List<NFATransition> trans;
		if (((Map<Integer, List<NFATransition>>) this.getTransitions())
				.containsKey(state)) {
			trans = (LinkedList<NFATransition>) this.getTransitions()
					.get(state);
		} else {
			trans = new LinkedList<NFATransition>();
			this.getTransitions().put(state, trans);
		}
		return trans;
	}

	public void createExitState(int exitState) {
		if (this.exitState != -1) {
			return;
		}
		this.exitState = exitState;
		if (!this.getStartState().equals(exitState)) {
			this.getTransitions().put(exitState,
					new LinkedList<NFATransition>());
			for (int i : this.getAccept()) {
				List<NFATransition> trans = getTransitionsOfState(i);
				trans.add(new NFATransition(null, exitState));
			}
		}

	}

	public Integer getExitState() {
		return exitState;
	}

	public void deleteExitState() {
		if (exitState == -1) {
			return;
		}
		for (Entry<Integer, List<NFATransition>> entry : ((Map<Integer, List<NFATransition>>) this
				.getTransitions()).entrySet()) {
			for (int i = 0; i < entry.getValue().size(); ++i) {
				if (entry.getValue().get(i).getTarget() == this.exitState) {
					entry.getValue().remove(i);
					--i;
				}
			}
		}
		this.getTransitions().remove(this.exitState);
		this.exitState = -1;
	}

	@Override
	public void addTransition(int start, int end, String key) {
		super.addTransition(start, end, key);
		Map<Integer, List<NFATransition>> trans = (Map<Integer, List<NFATransition>>) this
				.getTransitions();
		List<NFATransition> startTrans = getTransitionsOfState(start);
		startTrans.add(new NFATransition(key, end));
	}

	public void addTrans(Map.Entry<Integer, List<NFATransition>> tr) {
		// assert !trans.containsKey(tr.getKey());
		this.getTransitions().put(tr.getKey(), tr.getValue());
	}

	@Override
	public void addAcceptState(int state) {
		// TODO Auto-generated method stub
		super.addAcceptState(state);

		if (this.exitState != -1) {
			List<NFATransition> trans = this.getTransitionsOfState(state);
			if (trans == null) {
				trans = new LinkedList<NFATransition>();
				this.getTransitions().put(state, trans);
			}
			trans.add(new NFATransition(null, exitState));
		}
	}

	@Override
	public Map cloneTransitions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Simulator simulate(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		Map<Integer, List<NFATransition>> trans = (Map<Integer, List<NFATransition>>) this
				.getTransitions();
		String result = "{\n  start: " + Integer.toString(this.getStartState())
				+ ",\n  accept states: " + getAccept().toString()
				+ ",\n  transitions: [\n";
		for (int key : trans.keySet()) {
			for (NFATransition tran : trans.get(key)) {
				result += "  (" + Integer.toString(key) + ", "
						+ Integer.toString(tran.getTarget()) + ", \'"
						+ tran.getKey() + "\'),\n";
			}
		}
		result += "  ]\n}";
		return result;
	}
}
