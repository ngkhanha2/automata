package Automata.Input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import Automata.Automata;
import Automata.NFA.NFA;

public class NFAInput extends AutomataInput {

	@Override
	public Automata inputFronFile(String fileName) {
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return null;
		}

		NFA nfa = new NFA();

		String[] strings = readline(in);
		nfa.setStartState(Integer.parseInt(strings[0]));

		strings = readline(in);
		for (String s : strings) {
			nfa.addAcceptState(Integer.parseInt(s));
		}

		do {
			strings = readline(in);
			if (strings == null) {
				break;
			}
			int start = Integer.parseInt(strings[0]);
			int end = Integer.parseInt(strings[1]);
			nfa.addTransition(start, end, strings[2]);
		} while (true);

		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return nfa;
	}
}
