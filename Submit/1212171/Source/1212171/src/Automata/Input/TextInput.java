package Automata.Input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextInput {
	public String inputFromFile(String fileName) throws IOException {
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return null;
		}
		String s = "";
		String line;
		while ((line = in.readLine()) != null) {
			s += line;
		}
		return s;
	}
}
