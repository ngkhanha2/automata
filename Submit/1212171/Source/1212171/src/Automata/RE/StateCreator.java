package Automata.RE;

public class StateCreator {

	private int state;

	public StateCreator() {
		state = -1;
	}

	public int create() {
		++state;
		return state;
	}
}
