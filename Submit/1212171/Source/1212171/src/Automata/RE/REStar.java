package Automata.RE;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import Automata.NFA.NFA;
import Automata.NFA.NFATransition;

public class REStar implements RE {
	RE re;

	public REStar(RE re) {
		this.re = re;
	}

	@Override
	public NFA toNFA(StateCreator stateCreator) {
		NFA nfa1 = re.toNFA(stateCreator);
		Integer start = stateCreator.create();
		NFA nfa0 = new NFA();
		nfa0.setStartState(start);
		nfa0.createExitState(start);

		for (Entry<Integer, List<NFATransition>> entry : ((Map<Integer, List<NFATransition>>) nfa1
				.getTransitions()).entrySet()) {
			nfa0.addTrans(entry);
		}
		nfa0.addTransition(start, nfa1.getStartState(), null);
		nfa0.addTransition(nfa1.getExitState(), start, null);
		return nfa0;
	}
}
