package Automata.DFA;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import Automata.Pair;
import Automata.DFA.*;

class DFAMinimization {
	private DFA dfa;

	public DFAMinimization(DFA dfa) {
		this.dfa = dfa;

	}

	private int getNextState(Map<Integer, Map<String, Integer>> transitions,
			int state, String key) {
		Map<String, Integer> stateTrans = transitions.get(state);
		if (stateTrans == null) {
			return -1;
		}
		Integer nextState = stateTrans.get(key);
		if (nextState == null) {
			return -1;
		}
		return nextState;
	}

	private Map<Integer, Set<Integer>> getUnmarked(Set<String> characters,
			Map<Integer, Map<String, Integer>> transitions,
			Set<Integer> acceptStates) {

		Map<Integer, Map<Integer, Integer>> marked = new HashMap<Integer, Map<Integer, Integer>>();
		Map<Integer, Set<Integer>> unmarked = new HashMap<Integer, Set<Integer>>();

		for (int state : transitions.keySet()) {
			marked.put(state, new HashMap<Integer, Integer>());
			unmarked.put(state, new HashSet<Integer>());
		}

		for (int state : transitions.keySet()) {
			for (int state1 : transitions.keySet()) {
				if (state != state1) {
					if (!marked.get(state).containsKey(state1)
							&& !marked.get(state1).containsKey(state)
							&& acceptStates.contains(state)
							^ acceptStates.contains(state1) == true) {
						marked.get(state).put(state1, 0);
					}
				}
			}
		}

		boolean markExist = true;
		while (markExist) {
			markExist = false;
			for (int state : transitions.keySet()) {
				for (int state1 : transitions.keySet()) {
					if (state != state1
							&& !marked.get(state).containsKey(state1)
							&& !marked.get(state1).containsKey(state)) {
						boolean mark = false;
						for (String key : characters) {
							int p = getNextState(transitions, state, key);
							int q = getNextState(transitions, state1, key);
							if (marked.get(p).containsKey(q)) {
								marked.get(state).put(state1,
										marked.get(p).get(q) + 1);
								mark = true;
								break;
							}
							if (marked.get(q).containsKey(p)) {
								marked.get(state).put(state1,
										marked.get(q).get(p) + 1);
								mark = true;
								break;
							}
						}
						if (mark) {
							markExist = true;
							unmarked.get(state).remove(state1);
							unmarked.get(state1).remove(state);
						} else {
							if (!unmarked.get(state1).contains(state)) {
								unmarked.get(state).add(state1);
							}
						}
					}
				}
			}
		}

		return unmarked;
	}

	private void unionUnmarkedSet(Map<Integer, Set<Integer>> unmarkedSet) {
		boolean collected = false;
		while (!collected) {
			collected = true;
			int p = unmarkedSet.keySet().iterator().next();
			for (int q : unmarkedSet.get(p)) {
				if (unmarkedSet.containsKey(q)) {
					collected = false;
					for (int q1 : unmarkedSet.get(q)) {
						unmarkedSet.get(p).add(q1);
					}
					unmarkedSet.remove(q);
				}
			}
		}
	}

	private void unionState(Map<Integer, Map<String, Integer>> transitions,
			int p, int q) {
		for (int state : transitions.keySet()) {
			for (Entry<String, Integer> entry : transitions.get(state)
					.entrySet()) {
				if (entry.getValue() == q) {
					entry.setValue(p);
				}
			}
		}
		for (Entry<String, Integer> entry : transitions.get(q).entrySet()) {
			transitions.get(p).put(entry.getKey(), entry.getValue());
		}
	}

	public Map<Integer, Integer> createLabels(Integer startState,
			Map<Integer, Map<String, Integer>> transitions) {
		Map<Integer, Integer> labels = new HashMap<Integer, Integer>();
		labels.put(startState, 0);
		int label = 1;
		for (int key : transitions.keySet()) {
			if (!labels.containsKey(key)) {
				labels.put(key, label);
				++label;
			}
		}
		return labels;
	}

	private void reduce(Integer startState, Set<Integer> acceptStates,
			Map<Integer, Map<String, Integer>> transitions,
			Map<Integer, Set<Integer>> unmarkedSet) {
		unionUnmarkedSet(unmarkedSet);

		for (int p : unmarkedSet.keySet()) {
			for (int q : unmarkedSet.get(p)) {
				unionState(transitions, p, q);
				if (acceptStates.contains(q)) {
					acceptStates.remove(q);
					acceptStates.add(p);
				}
				if (q == startState) {
					startState = p;
				}
			}
		}

		for (int p : unmarkedSet.keySet()) {
			for (int q : unmarkedSet.get(p)) {
				transitions.remove(q);
			}
		}
	}

	private Pair<Integer, Pair<Set<Integer>, Map<Integer, Map<String, Integer>>>> label(
			Integer startState, Set<Integer> acceptStates,
			Map<Integer, Map<String, Integer>> transitions) {
		Map<Integer, Integer> labels = createLabels(startState, transitions);
		Map<Integer, Map<String, Integer>> newTransitions = new HashMap<Integer, Map<String, Integer>>();
		for (Entry<Integer, Map<String, Integer>> entry : transitions
				.entrySet()) {
			Map<String, Integer> trans = new HashMap<String, Integer>();
			newTransitions.put(labels.get(entry.getKey()), trans);
			for (Entry<String, Integer> e : entry.getValue().entrySet()) {
				trans.put(e.getKey(), labels.get(e.getValue()));
			}
		}
		Set<Integer> newAcceptStates = new HashSet<Integer>();
		for (int s : acceptStates) {
			newAcceptStates.add(labels.get(s));
		}
		return new Pair<Integer, Pair<Set<Integer>, Map<Integer, Map<String, Integer>>>>(
				labels.get(startState),
				new Pair<Set<Integer>, Map<Integer, Map<String, Integer>>>(
						newAcceptStates, newTransitions));

	}

	public DFA minimize() {
		// Clone to new DFA
		Set<String> characters = this.dfa.cloneCharacters();
		int startState = this.dfa.getStartState();
		Set<Integer> acceptStates = this.dfa.cloneAcceptStates();
		Map<Integer, Map<String, Integer>> transitions = (Map<Integer, Map<String, Integer>>) this.dfa
				.cloneTransitions();

		reduce(startState, acceptStates, transitions,
				getUnmarked(characters, transitions, acceptStates));

		Pair<Integer, Pair<Set<Integer>, Map<Integer, Map<String, Integer>>>> result = label(
				startState, acceptStates, transitions);

		return new DFA(result.getL(), result.getR().getL(), result.getR()
				.getR());
	}
}
