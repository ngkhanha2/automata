package Automata;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class Automata {
	private int startState;

	private Map trans;

	private Set<Integer> acceptStates;

	private Set<String> characters;

	public Automata(Map trans) {
		this.trans = trans;
		this.startState = -1;
		this.acceptStates = new HashSet<Integer>();
		this.characters = new HashSet<String>();
	}

	public Automata(Set<Integer> acceptStates, Map trans) {
		this.trans = trans;
		this.startState = -1;
		this.acceptStates = acceptStates;
		this.characters = new HashSet<String>();
	}

	public void setStartState(int start) {
		this.startState = start;
	}

	public Integer getStartState() {
		return startState;
	}

	public Map getTransitions() {
		return trans;
	}

	public void addAcceptState(int state) {
		this.acceptStates.add(state);
	}

	public Set<Integer> getAccept() {
		return acceptStates;
	}

	public void addTransition(int start, int end, String key) {
		this.addCharacter(key);
	}

	public void addCharacter(String key) {
		if (key != null && !this.characters.contains(key)) {
			this.characters.add(key);
		}
	}

	public Set<String> getCharacters() {
		return this.characters;
	}

	public abstract Map cloneTransitions();

	public Set<Integer> cloneAcceptStates() {
		Set<Integer> acceptStates = new HashSet<Integer>();
		for (int key : this.acceptStates) {
			acceptStates.add(key);
		}
		return acceptStates;
	}

	public Set<String> cloneCharacters() {
		Set<String> characters = new HashSet<String>();
		for (String key : this.characters) {
			characters.add(key);
		}
		return characters;
	}

	public abstract Simulator simulate(String text);

	public abstract String toString();
}
