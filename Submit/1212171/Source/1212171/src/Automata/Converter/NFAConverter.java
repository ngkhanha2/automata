package Automata.Converter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import Automata.DFA.DFA;
import Automata.NFA.NFA;
import Automata.NFA.NFATransition;

public class NFAConverter {

	private NFA nfa;

	public NFAConverter(NFA nfa) {
		this.nfa = nfa;
	}

	private Set<Integer> epsilonClose(Set<Integer> S,
			Map<Integer, List<NFATransition>> trans) {
		Queue<Integer> queue = new LinkedList<Integer>(S);
		Set<Integer> res = new HashSet<Integer>(S);
		while (!queue.isEmpty()) {
			Integer s = queue.remove();
			for (NFATransition tr : trans.get(s)) {
				if (tr.getKey() == null && !res.contains(tr.getTarget())) {
					res.add(tr.getTarget());
					queue.add(tr.getTarget());
				}
			}
		}
		return res;
	}

	private Map<Set<Integer>, Map<String, Set<Integer>>> compositeDFATransitiions(
			Integer s0, Map<Integer, List<NFATransition>> NFATransitions) {
		Set<Integer> startState = epsilonClose(Collections.singleton(s0),
				NFATransitions);
		Queue<Set<Integer>> queue = new LinkedList<Set<Integer>>();
		queue.add(startState);
		Map<Set<Integer>, Map<String, Set<Integer>>> res = new HashMap<Set<Integer>, Map<String, Set<Integer>>>();

		while (!queue.isEmpty()) {
			Set<Integer> states = queue.remove();
			if (!res.containsKey(states)) {
				Map<String, Set<Integer>> trans = new HashMap<String, Set<Integer>>();
				for (Integer s : states) {
					for (NFATransition tr : NFATransitions.get(s)) {
						if (tr.getKey() != null) {
							Set<Integer> toState;
							if (trans.containsKey(tr.getKey()))
								toState = trans.get(tr.getKey());
							else {
								toState = new HashSet<Integer>();
								trans.put(tr.getKey(), toState);
							}
							toState.add(tr.getTarget());
						}
					}
				}
				HashMap<String, Set<Integer>> transClosed = new HashMap<String, Set<Integer>>();
				for (Map.Entry<String, Set<Integer>> entry : trans.entrySet()) {
					Set<Integer> Tclose = epsilonClose(entry.getValue(),
							NFATransitions);
					transClosed.put(entry.getKey(), Tclose);
					queue.add(Tclose);
				}
				res.put(states, transClosed);
			}
		}
		return res;
	}

	private Map<Set<Integer>, Integer> createLabels(
			Collection<Set<Integer>> states) {
		Map<Set<Integer>, Integer> labels = new HashMap<Set<Integer>, Integer>();
		for (Set<Integer> k : states)
			labels.put(k, labels.size());
		return labels;
	}

	private Map<Integer, Map<String, Integer>> label(
			Map<Set<Integer>, Integer> labels,
			Map<Set<Integer>, Map<String, Set<Integer>>> compositeTransitiions) {

		Map<Integer, Map<String, Integer>> trans = new HashMap<Integer, Map<String, Integer>>();
		for (Map.Entry<Set<Integer>, Map<String, Set<Integer>>> entry : compositeTransitiions
				.entrySet()) {
			Set<Integer> k = entry.getKey();
			Map<String, Integer> newktrans = new HashMap<String, Integer>();
			for (Map.Entry<String, Set<Integer>> tr : entry.getValue()
					.entrySet())
				newktrans.put(tr.getKey(), labels.get(tr.getValue()));
			trans.put(labels.get(k), newktrans);
		}

		return trans;
	}

	private Set<Integer> getAcceptStates(Set<Set<Integer>> states,
			Map<Set<Integer>, Integer> renamer, Integer exit) {
		Set<Integer> acceptStates = new HashSet<Integer>();
		for (Set<Integer> state : states)
			if (state.contains(exit))
				acceptStates.add(renamer.get(state));
		return acceptStates;
	}

	public DFA toDFA() {
		nfa.createExitState();
		Map<Set<Integer>, Map<String, Set<Integer>>> cDFATrans = compositeDFATransitiions(
				nfa.getStartState(), nfa.getTransitions());

		Set<Integer> cStartState = epsilonClose(
				Collections.singleton(nfa.getStartState()),
				nfa.getTransitions());

		Set<Set<Integer>> cDfaStates = cDFATrans.keySet();

		Map<Set<Integer>, Integer> labels = createLabels(cDfaStates);
		Map<Integer, Map<String, Integer>> DFATransitions = label(labels,
				cDFATrans);

		int startState = labels.get(cStartState);
		Set<Integer> accepStates = getAcceptStates(cDfaStates, labels,
				nfa.getExitState());

		nfa.deleteExitState();

		return new DFA(startState, accepStates, DFATransitions);
	}

}
