package Automata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import Automata.Converter.DFAConverter;
import Automata.Converter.NFAConverter;
import Automata.Converter.REConverter;
//import Automata.Converter.REConverter;
import Automata.DFA.DFA;
import Automata.Input.DFAInput;
import Automata.Input.NFAInput;
import Automata.Input.TextInput;
import Automata.NFA.NFA;
import Automata.Output.DFAOutput;
import Automata.Output.TextOutput;

public class Main {
	private static void dfaSimulator(BufferedReader in) throws IOException {
		System.out.print("Input DFA file: ");
		String fileName = in.readLine();
		Automata dfa = (new DFAInput()).inputFronFile(fileName);
		if (dfa != null) {
			System.out.println("DFA: ");
			System.out.println(dfa);
			System.out.print("Input text file: ");
			fileName = in.readLine();
			String text = (new TextInput()).inputFromFile(fileName);
			if (text != null) {
				Simulator s = dfa.simulate(text);
				System.out.println("Text: " + text);
				System.out.println("Result: ");
				System.out.println(s);
				System.out
						.print("Output to file? (Write 'true' if you want): ");
				boolean out = Boolean.parseBoolean(in.readLine());
				if (out) {
					System.out.print("Output file: ");
					fileName = in.readLine();
					(new TextOutput()).outputToFile(s.toString(), fileName);
				}
			} else {
				System.out.println("Cannot read text file.");
			}
		} else {
			System.out.println("Cannot read DFA file.");
		}
	}

	private static void convertNFAToDFA(BufferedReader in) throws IOException {
		System.out.print("Input NFA file: ");
		String fileName = in.readLine();
		NFA nfa = (NFA) (new NFAInput()).inputFronFile(fileName);
		if (nfa != null) {
			System.out.println("NFA: ");
			System.out.println(nfa);
			DFA dfa = (new NFAConverter(nfa)).toDFA();
			System.out.println("Result: ");
			System.out.println(dfa);
			System.out.print("Output to file? (Write 'true' if you want): ");
			boolean out = Boolean.parseBoolean(in.readLine());
			if (out) {
				System.out.print("Output file: ");
				fileName = in.readLine();
				(new DFAOutput()).outputToFile(dfa, fileName);
			}
		} else {
			System.out.println("Cannot read NFA file.");
		}
	}

	private static void minimizeDFA(BufferedReader in) throws IOException {
		// TODO Auto-generated method stub
		System.out.print("Input DFA file: ");
		String fileName = in.readLine();
		DFA dfa = (DFA) (new DFAInput()).inputFronFile(fileName);
		if (dfa != null) {
			System.out.println("DFA: ");
			System.out.println(dfa);
			DFA dfa1 = dfa.minimize();
			System.out.println("Result: ");
			System.out.println(dfa1);
			System.out.print("Output to file? (Write 'true' if you want): ");
			boolean out = Boolean.parseBoolean(in.readLine());
			if (out) {
				System.out.print("Output file: ");
				fileName = in.readLine();
				(new DFAOutput()).outputToFile(dfa1, fileName);
			}
		} else {
			System.out.println("Cannot read DFA file.");
		}
	}

	private static void convertDFAToRE(BufferedReader in) throws IOException {
		// TODO Auto-generated method stub
		System.out.print("Input DFA file: ");
		String fileName = in.readLine();
		DFA dfa = (DFA) (new DFAInput()).inputFronFile(fileName);
		if (dfa != null) {
			System.out.println("DFA: ");
			System.out.println(dfa);
			String re = (new DFAConverter(dfa)).toRE();
			System.out.println("Result: " + re);
			System.out.print("Output to file? (Write 'true' if you want): ");
			boolean out = Boolean.parseBoolean(in.readLine());
			if (out) {
				System.out.print("Output file: ");
				fileName = in.readLine();
				(new TextOutput()).outputToFile(re, fileName);
			}
		} else {
			System.out.println("Cannot read DFA file.");
		}
	}

	private static void convertREToDFA(BufferedReader in) throws IOException {
		// TODO Auto-generated method stub
		System.out.print("Input RE file: ");
		String fileName = in.readLine();
		String re = (new TextInput()).inputFromFile(fileName);
		if (re != null) {
			System.out.println("RE: " + re);
			DFA dfa = (new REConverter(re)).toDFA();
			System.out.println("Result: ");
			System.out.println(dfa);
			System.out.print("Output to file? (Write 'true' if you want): ");
			boolean out = Boolean.parseBoolean(in.readLine());
			if (out) {
				System.out.print("Output file: ");
				fileName = in.readLine();
				(new DFAOutput()).outputToFile(dfa, fileName);
			}
		} else {
			System.out.println("Cannot read RE file.");
		}
	}

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out
				.print("1. DFA simulator.\n2. Convert NFA to DFA.\n3. Convert RE to DFA.\n4. Convert DFA to RE.\n5. Minimize DFA.\nChoose: ");
		int c = Integer.parseInt(in.readLine());
		switch (c) {
		case 1:
			dfaSimulator(in);
			break;
		case 2:
			convertNFAToDFA(in);
			break;
		case 3:
			convertREToDFA(in);
			break;
		case 4:
			convertDFAToRE(in);
			break;
		case 5:
			minimizeDFA(in);
			break;
		}
	}
}
