package Automata.Converter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;

import Automata.Pair;
import Automata.DFA.DFA;

public class DFAConverter {

	private DFA dfa;

	public DFAConverter(DFA dfa) {
		this.dfa = dfa;
	}

	// Convert DFA to RE
	private void addTransition(Map<Integer, Map<String, Integer>> transitions,
			int start, int end, String key) {
		Map<String, Integer> trans;
		if (!transitions.containsKey(start)) {
			trans = new HashMap<String, Integer>();
			transitions.put(start, trans);
		} else {
			trans = transitions.get(start);
		}
		trans.put(key, end);
		if (!transitions.containsKey(end)) {
			transitions.put(end, new HashMap<String, Integer>());
		}
	}

	private String reprocessKey(String key) {
		if (key == null) {
			return Character.toString((char) 157);
		}
		return key;
	}

	private void addTransitionString(
			Map<Integer, Pair<Integer, String>> trasitionStrings, int state,
			String key) {

		String k = reprocessKey(key);

		Pair<Integer, String> transString = trasitionStrings.get(state);

		if (transString == null) {
			transString = new Pair<Integer, String>(1, k);
			trasitionStrings.put(state, transString);
		} else {
			transString.setL(transString.getL() + 1);
			transString.setR(transString.getR() + "+" + k);
		}
	}

	private Map<Integer, String> getTransitionStrings(
			Map<Integer, Pair<Integer, String>> trasitionStrings) {
		Map<Integer, String> result = new HashMap<Integer, String>();
		for (Entry<Integer, Pair<Integer, String>> entry : trasitionStrings
				.entrySet()) {
			if (entry.getValue().getL() > 1) {
				result.put(entry.getKey(), "(" + entry.getValue().getR() + ")");
			} else {
				result.put(entry.getKey(), entry.getValue().getR());
			}
		}
		return result;
	}

	private Map<Integer, String> getTransitionStringsOut(
			Map<Integer, Map<String, Integer>> transitions, int state) {
		Map<String, Integer> trans = transitions.get(state);
		if (trans == null) {
			return null;
		}

		Map<Integer, Pair<Integer, String>> transOut = new HashMap<Integer, Pair<Integer, String>>();

		for (Entry<String, Integer> entry : trans.entrySet()) {
			this.addTransitionString(transOut, entry.getValue(), entry.getKey());
		}

		return getTransitionStrings(transOut);
	}

	private Map<Integer, String> getTransitionStringsIn(
			Map<Integer, Map<String, Integer>> transitions, int state) {

		Map<Integer, Pair<Integer, String>> transIn = new HashMap<Integer, Pair<Integer, String>>();

		for (Entry<Integer, Map<String, Integer>> entry : transitions
				.entrySet()) {
			for (Entry<String, Integer> e : entry.getValue().entrySet()) {
				if (e.getValue() == state) {
					addTransitionString(transIn, entry.getKey(), e.getKey());
				}
			}
		}

		return getTransitionStrings(transIn);
	}

	public String toRE() {
		Queue<Integer> queue = new LinkedList<Integer>();

		// Clone to GNFA
		Map<Integer, Map<String, Integer>> gNFATransitions = this.dfa
				.cloneTransitions();

		for (int key : gNFATransitions.keySet()) {
			queue.add(key);
		}

		int startState = gNFATransitions.size() + 1;
		addTransition(gNFATransitions, startState, dfa.getStartState(), null);

		int acceptState = startState + 1;
		for (int state : dfa.getAccept()) {
			addTransition(gNFATransitions, state, acceptState, null);
		}

		// Convert to RE
		while (!queue.isEmpty()) {
			Integer current = queue.remove();

			Map<Integer, String> transOut = getTransitionStringsOut(
					gNFATransitions, current);
			gNFATransitions.remove(current);

			if (transOut.containsKey(current)) {
				String currentTrans = "(" + transOut.get(current) + ")*";
				transOut.remove(current);
				for (Entry<Integer, String> entry : transOut.entrySet()) {
					entry.setValue(currentTrans + entry.getValue());
				}
			}

			Map<Integer, String> transIn = getTransitionStringsIn(
					gNFATransitions, current);

			for (Entry<Integer, Map<String, Integer>> entry : gNFATransitions
					.entrySet()) {
				boolean exist = true;
				while (exist) {
					exist = false;
					for (Entry<String, Integer> e : entry.getValue().entrySet()) {
						if (e.getValue() == current) {
							entry.getValue().remove(e.getKey());
							exist = true;
							break;
						}
					}
				}
			}

			for (Entry<Integer, String> entryIn : transIn.entrySet()) {
				for (Entry<Integer, String> entryOut : transOut.entrySet()) {

					addTransition(gNFATransitions, entryIn.getKey(),
							entryOut.getKey(),
							entryIn.getValue() + entryOut.getValue());
				}
			}
		}

		// Get result
		String result = null;
		for (String key : gNFATransitions.get(startState).keySet()) {
			if (result == null) {
				result = key;

			} else {
				result += "+" + key;
			}
		}
		return result;
	}
}
