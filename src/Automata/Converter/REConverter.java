package Automata.Converter;

import java.util.Stack;

import Automata.DFA.DFA;
import Automata.NFA.NFA;
import Automata.RE.RE;
import Automata.RE.REConcatenation;
import Automata.RE.REStar;
import Automata.RE.RESymbol;
import Automata.RE.REUnion;
import Automata.RE.StateCreator;

public class REConverter {
	private String re;

	public REConverter(String re) {
		this.re = re;
	}

	private boolean isOperator(char c) {
		switch (c) {
		case '\0':
		case '+':
			return true;
		}
		return false;
	}

	private int getPriority(char c) {
		switch (c) {
		case '\0':
			return 1;
		}
		return 0;
	}

	public String getPostfix() {
		// Initialize Infix
		String infix = Character.toString(re.charAt(0));
		for (int i = 1; i < re.length(); ++i) {
			int pi = i - 1;
			if (re.charAt(i) != '*' && re.charAt(i) != ')'
					&& re.charAt(pi) != '(' && re.charAt(i) != '+'
					&& re.charAt(pi) != '+') {
				infix += "\0";
			}
			infix += re.charAt(i);
		}

		String postfix = "";
		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i < infix.length(); ++i) {
			char c = infix.charAt(i);
			char x;
			switch (c) {
			case '(':
				stack.push(c);
				break;
			case ')':
				x = stack.pop();
				while (x != '(') {
					postfix += x;
					x = stack.pop();
				}
				break;
			default:
				if (isOperator(c)) {
					while (stack.size() > 0
							&& this.isOperator(stack.peek())
							&& this.getPriority(c) <= this.getPriority(stack
									.peek())) {
						postfix += stack.pop();
					}
					stack.push(c);
				} else {
					postfix += c;
				}
				break;
			}
		}
		while (stack.size() > 0) {
			postfix += stack.pop();
		}
		return postfix;
	}

	public NFA toNFA() {
		String postfix = this.getPostfix();
		Stack<RE> stack = new Stack<RE>();
		for (int i = 0; i < postfix.length(); ++i) {
			char c = postfix.charAt(i);
			RE re2, re1;
			switch (c) {
			case '\0':
				re2 = stack.pop();
				re1 = stack.pop();
				stack.push(new REConcatenation(re1, re2));
				break;
			case '*':
				re1 = stack.pop();
				stack.push(new REStar(re1));
				break;
			case '+':
				re2 = stack.pop();
				re1 = stack.pop();
				stack.push(new REUnion(re1, re2));
				break;
			default:
				if (c == (char) 157) {
					stack.push(new RESymbol(null));
				}
				stack.push(new RESymbol(Character.toString(c)));
				break;
			}
		}
		StateCreator sc = new StateCreator();
		return stack.pop().toNFA(sc);
	}

	public DFA toDFA() {
		NFA nfa = toNFA();
		return (new NFAConverter(nfa)).toDFA();
	}
}
