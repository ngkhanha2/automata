package Automata.RE;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import Automata.NFA.NFA;
import Automata.NFA.NFATransition;

public class REUnion implements RE {
	RE re1, re2;

	public REUnion(RE re1, RE re2) {
		this.re1 = re1;
		this.re2 = re2;
	}

	public NFA toNFA(StateCreator stateCreator) {
		// TODO Auto-generated method stub
		NFA nfa1 = re1.toNFA(stateCreator);
		NFA nfa2 = re2.toNFA(stateCreator);
		Integer start = stateCreator.create();
		Integer end = stateCreator.create();
		NFA nfa0 = new NFA();
		nfa0.setStartState(start);
		nfa0.createExitState(end);

		for (Entry<Integer, List<NFATransition>> entry : ((Map<Integer, List<NFATransition>>) nfa1
				.getTransitions()).entrySet()) {
			nfa0.addTrans(entry);
		}

		for (Entry<Integer, List<NFATransition>> entry : ((Map<Integer, List<NFATransition>>) nfa2
				.getTransitions()).entrySet()) {
			nfa0.addTrans(entry);
		}

		nfa0.addTransition(start, nfa1.getStartState(), null);
		nfa0.addTransition(start, nfa2.getStartState(), null);
		nfa0.addTransition(nfa1.getExitState(), end, null);
		nfa0.addTransition(nfa2.getExitState(), end, null);
		return nfa0;
	}

}
