package Automata.RE;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import Automata.NFA.NFA;
import Automata.NFA.NFATransition;

public class REConcatenation implements RE {
	RE re1, re2;

	public REConcatenation(RE re1, RE re2) {
		this.re1 = re1;
		this.re2 = re2;
	}

	@Override
	public NFA toNFA(StateCreator stateCreator) {
		NFA nfa1 = re1.toNFA(stateCreator);
		NFA nfa2 = re2.toNFA(stateCreator);
		NFA nfa0 = new NFA();
		nfa0.setStartState(nfa1.getStartState());
		nfa0.createExitState(nfa2.getExitState());

		for (Entry<Integer, List<NFATransition>> entry : ((Map<Integer, List<NFATransition>>) nfa1
				.getTransitions()).entrySet()) {
			nfa0.addTrans(entry);
		}

		for (Entry<Integer, List<NFATransition>> entry : ((Map<Integer, List<NFATransition>>) nfa2
				.getTransitions()).entrySet()) {
			nfa0.addTrans(entry);
		}

		nfa0.addTransition(nfa1.getExitState(), nfa2.getStartState(), null);
		return nfa0;
	}
}
