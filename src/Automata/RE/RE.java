package Automata.RE;

import Automata.NFA.NFA;

public interface RE {

	public NFA toNFA(StateCreator stateCreator);
}
