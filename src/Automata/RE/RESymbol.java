package Automata.RE;

import Automata.NFA.NFA;

public class RESymbol implements RE {
	String symbol;

	public RESymbol(String symbol) {
		this.symbol = symbol;
	}

	@Override
	public NFA toNFA(StateCreator stateCreator) {
		// TODO Auto-generated method stub
		NFA nfa = new NFA();
		int start = stateCreator.create();
		int end = stateCreator.create();
		nfa.setStartState(start);
		nfa.createExitState(end);
		nfa.addTransition(start, end, symbol);
		return nfa;
	}
}
