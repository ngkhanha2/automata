package Automata.DFA;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import Automata.Automata;
import Automata.Simulator;

public class DFA extends Automata {

	public DFA() {
		super(new HashMap<Integer, Map<String, Integer>>());
	}

	public DFA(Integer startState, Set<Integer> acceptStates,
			Map<Integer, Map<String, Integer>> trans) {
		super(acceptStates, trans);
		for (int key : trans.keySet()) {
			for (String s : trans.get(key).keySet()) {
				this.addCharacter(s);
			}
		}
		this.setStartState(startState);
	}

	@Override
	public void addTransition(int start, int end, String key) {
		// TODO Auto-generated method stub
		super.addTransition(start, end, key);
		Map<String, Integer> t = (Map<String, Integer>) this.getTransitions()
				.get(start);
		if (t == null) {
			t = new HashMap<String, Integer>();
			this.getTransitions().put(start, t);
		}
		t.put(key, end);

	}

	@Override
	public Map cloneTransitions() {
		// TODO Auto-generated method stub
		Map<Integer, Map<String, Integer>> transitions = new HashMap<Integer, Map<String, Integer>>();

		for (Entry<Integer, Map<String, Integer>> entry : ((Map<Integer, Map<String, Integer>>) this
				.getTransitions()).entrySet()) {
			for (Entry<String, Integer> e : entry.getValue().entrySet()) {
				Map<String, Integer> trans;
				if (!transitions.containsKey(entry.getKey())) {
					trans = new HashMap<String, Integer>();
					transitions.put(entry.getKey(), trans);
				} else {
					trans = transitions.get(entry.getKey());
				}
				trans.put(e.getKey(), e.getValue());
				if (!transitions.containsKey(e.getValue())) {
					transitions.put(e.getValue(),
							new HashMap<String, Integer>());
				}
			}
		}

		return transitions;
	}

	public DFA minimize() {
		DFAMinimization dfaMinimization = new DFAMinimization(this);
		return dfaMinimization.minimize();
	}

	@Override
	public Simulator simulate(String text) {
		// TODO Auto-generated method stub
		return new DFASimulator(this, text);
	}

	@Override
	public String toString() {
		Map<Integer, Map<String, Integer>> trans = (Map<Integer, Map<String, Integer>>) this
				.getTransitions();
		String result = "{\n  start: " + Integer.toString(this.getStartState())
				+ ",\n  accept states: " + getAccept().toString()
				+ ",\n  transitions: [\n";
		for (int key : trans.keySet()) {
			for (Entry<String, Integer> entry : trans.get(key).entrySet()) {
				result += "  (" + Integer.toString(key) + ", "
						+ Integer.toString(entry.getValue()) + ", \'"
						+ entry.getKey() + "\'),\n";
			}
		}
		result += "  ]\n}";
		return result;
	}
}
