package Automata.DFA;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import Automata.Pair;
import Automata.Simulator;

public class DFASimulator implements Simulator {
	private DFA dfa;

	private String text;

	private List<Pair<Integer, Boolean>> result;

	public DFASimulator(DFA dfa, String text) {
		this.dfa = dfa;
		this.text = text;
		this.result = new ArrayList<Pair<Integer, Boolean>>();
		this.simulate();
	}

	private void simulate() {
		Map<Integer, Map<String, Integer>> trans = ((Map<Integer, Map<String, Integer>>) this.dfa
				.getTransitions());
		Integer state = this.dfa.getStartState();

		result.add(new Pair<Integer, Boolean>(state, this.dfa.getAccept()
				.contains(state)));

		for (int i = 0; i < this.text.length(); ++i) {
			state = trans.get(state).get(
					Character.toString(this.text.charAt(i)));
			if (state == null) {
				result.add(new Pair<Integer, Boolean>(-1, false));
				break;
			}
			result.add(new Pair<Integer, Boolean>(state, this.dfa.getAccept()
					.contains(state)));
		}
	}

	@Override
	public String toString() {
		String s = "{\n  steps: [\n";
		int step = 0;
		boolean accepted = true;
		for (Pair<Integer, Boolean> p : this.result) {
			s += "    (step: " + Integer.toString(step) + ", state: "
					+ Integer.toString(p.getL()) + "),\n";
			accepted = p.getR();
			++step;
		}
		s += "  ],\n  accepted: " + Boolean.toString(accepted) + "\n}";
		return s;
	}
}
