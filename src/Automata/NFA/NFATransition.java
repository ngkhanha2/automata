package Automata.NFA;

public class NFATransition {
	private String key;
	private Integer target;

	public NFATransition(String key, Integer target) {
		this.setKey(key);
		this.setTarget(target);
	}

	public String toString() {
		return "-" + this.getKey() + "-> " + this.getTarget();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getTarget() {
		return target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}
}
