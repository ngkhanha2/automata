package Automata.Input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import Automata.Automata;
import Automata.DFA.DFA;

public class DFAInput extends AutomataInput {

	@Override
	public Automata inputFronFile(String fileName) {
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return null;
		}

		DFA dfa = new DFA();

		String[] strings = readline(in);
		dfa.setStartState(Integer.parseInt(strings[0]));

		strings = readline(in);
		for (String s : strings) {
			dfa.addAcceptState(Integer.parseInt(s));
		}

		do {
			strings = readline(in);
			if (strings == null) {
				break;
			}
			int start = Integer.parseInt(strings[0]);
			int end = Integer.parseInt(strings[1]);
			dfa.addTransition(start, end, strings[2]);
		} while (true);

		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dfa;
	}
}
