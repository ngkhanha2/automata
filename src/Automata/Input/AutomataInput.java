package Automata.Input;

import java.io.BufferedReader;

import Automata.Automata;
import Automata.DFA.DFA;

public abstract class AutomataInput {

	protected String[] readline(BufferedReader in) {
		String line;
		String[] strings;
		try {
			line = in.readLine();
			strings = line.split(" ");
			return strings;
		} catch (Exception ex) {

		}
		return null;
	}

	public abstract Automata inputFronFile(String fileName);
}
