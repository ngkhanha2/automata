package Automata.Output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import Automata.DFA.DFA;

public class DFAOutput {

	public void outputToFile(DFA dfa, String fileName) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
		out.write(Integer.toString(dfa.getStartState()) + "\n");
		for (int s : dfa.getAccept()) {
			out.write(Integer.toString(s) + " ");
		}
		out.write("\n");
		Map<Integer, Map<String, Integer>> transitions = dfa.getTransitions();

		for (int key : transitions.keySet()) {
			for (Entry<String, Integer> entry : transitions.get(key).entrySet()) {
				out.write(Integer.toString(key) + " "
						+ Integer.toString(entry.getValue()) + " "
						+ entry.getKey() + "\n");
			}
		}
		out.close();
	}
}
